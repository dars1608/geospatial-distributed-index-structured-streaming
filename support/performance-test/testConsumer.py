#Run pip install kafka-python

from argparse import ArgumentParser
from kafka import KafkaConsumer

parser = ArgumentParser(prog="kafka-test-consumer", description="Simple Kafka consumer which prints messages to std out")

parser.add_argument("topic",  help="Kafka topic.")
parser.add_argument("-b", "--bootstrap_servers", nargs='*', default=["localhost:9092"], help="List of Kafka bootstrap servers.")
parser.add_argument("-g", "--group_id", required=True, help="Kafka group ID.")

args = parser.parse_args()


consumer = KafkaConsumer(
     args.topic,
     bootstrap_servers=args.bootstrap_servers,
     auto_offset_reset='latest',
     enable_auto_commit=True,
     group_id=args.group_id,
     value_deserializer=lambda x: x)
     
for message in consumer:
    print(message.value)