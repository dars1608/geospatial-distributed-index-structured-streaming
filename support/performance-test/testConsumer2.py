#Run pip install kafka-python

import json
from argparse import ArgumentParser
from kafka import KafkaConsumer

parser = ArgumentParser(prog="kafka-test-consumer-2", description="Simple Kafka consumer which prints messages to std out")

parser.add_argument("topic",  help="Kafka topic.")
parser.add_argument("-b", "--bootstrap_servers", nargs='*', default=["localhost:9092"], help="List of Kafka bootstrap servers.")
parser.add_argument("-g", "--group_id", required=True, help="Kafka group ID.")

args = parser.parse_args()


consumer = KafkaConsumer(
	 args.topic,
	 bootstrap_servers=args.bootstrap_servers,
	 auto_offset_reset='latest',
	 enable_auto_commit=True,
	 group_id=args.group_id,
	 value_deserializer=lambda x: x,
	 consumer_timeout_ms=300000)

with open("counterfile.csv", "w") as outfile:
	for message in consumer:
		try:
			d = json.loads(message.value)
			
			outfile.write(str(d["subscriptionId"]))
			outfile.write(",")
			outfile.write(str(d["dataId"]))
			outfile.write("\n")
		except Exception as e:
			print("Fail", e)
		

		

