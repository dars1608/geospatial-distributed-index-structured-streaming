#Run pip install kafka-python

from argparse import ArgumentParser, FileType
from kafka import KafkaProducer

#curr_time = lambda: int(round(time() * 1000))

parser = ArgumentParser(prog="kafka-test-producer", description="Simple Kafka producer which reads data from file and pushes it on the topic")

parser.add_argument("input_file", type=FileType("r"), help="Input file - one message per line.")
parser.add_argument("-b", "--bootstrap_servers", nargs='*', default=["localhost:9092"], help="List of Kafka bootstrap servers.")
parser.add_argument("-t", "--topics", nargs='*', default=[], help="List of Kafka topics.")
#parser.add_argument("-d", "--delay", default=1000, type=int, help="Delay in miliseconds between 2 messages (positive values required).")

args = parser.parse_args()
producer = KafkaProducer(bootstrap_servers=args.bootstrap_servers, value_serializer=lambda x: x.encode('utf-8'))

#last_timestamp = curr_time()
for message in args.input_file:
    #time_to_wait = args.delay - (curr_time() - last_timestamp)
    #if time_to_wait > 0:
        #sleep(time_to_wait/1000) #sleep in miliseconds
	
    for topic in args.topics:
        producer.send(topic, value=message)
    
    #last_timestamp = curr_time()

args.input_file.close()