name := "geospatial-index-structured-streaming"

version := "0.1"

scalaVersion := "2.11.12"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

// https://mvnrepository.com/artifact/org.apache.spark/spark-core
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.4"

// https://mvnrepository.com/artifact/org.apache.spark/spark-sql
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.3.4"

// https://mvnrepository.com/artifact/org.apache.spark/spark-sql-kafka-0-10
libraryDependencies += "org.apache.spark" %% "spark-sql-kafka-0-10" % "2.3.4" excludeAll(ExclusionRule(organization = "net.jpountz.lz4", name = "lz4"))

// https://mvnrepository.com/artifact/org.datasyslab/geospark
libraryDependencies += "org.datasyslab" % "geospark" % "1.2.0"

// https://mvnrepository.com/artifact/org.datasyslab/geospark-sql
libraryDependencies += "org.datasyslab" %% "geospark-sql_2.3" % "1.2.0" from "https://repo1.maven.org/maven2/org/datasyslab/geospark-sql_2.3/1.2.0/geospark-sql_2.3-1.2.0.jar"

// https://mvnrepository.com/artifact/org.apache.hadoop/hadoop-mapreduce-client-core
dependencyOverrides += "org.apache.hadoop" % "hadoop-mapreduce-client-core" % "3.1.3"

// https://mvnrepository.com/artifact/org.apache.hadoop/hadoop-common
dependencyOverrides += "org.apache.hadoop" % "hadoop-common" % "3.1.3"

// https://mvnrepository.com/artifact/commons-io/commons-io
dependencyOverrides += "commons-io" % "commons-io" % "2.6"

// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.6.5"

// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.5"

// https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-scala_2.11
dependencyOverrides += "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.6.5"