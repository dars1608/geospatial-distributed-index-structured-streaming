package hr.fer.ztel.geospatial_structured_streaming.stats

import org.apache.spark.sql.streaming.StreamingQueryProgress

class StreamingQueryProgressWrapper(info: StreamingQueryProgress) {

  info.toString()

  override def toString: String = {
    s"${info.timestamp}\t${info.numInputRows}\t${info.numInputRows / info.inputRowsPerSecond}\t${info.inputRowsPerSecond / 1000}"
  }
}
