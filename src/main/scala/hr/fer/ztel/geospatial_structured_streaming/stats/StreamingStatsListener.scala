package hr.fer.ztel.geospatial_structured_streaming.stats

import org.apache.spark.sql.streaming.StreamingQueryListener

import scala.collection.mutable.ListBuffer

class StreamingStatsListener extends StreamingQueryListener {

  val streamingProgressList = new ListBuffer[StreamingQueryProgressWrapper]

  override def onQueryStarted(event: StreamingQueryListener.QueryStartedEvent): Unit = {}

  override def onQueryProgress(event: StreamingQueryListener.QueryProgressEvent): Unit = {
    val info = event.progress
    if (info.numInputRows != 0)
      streamingProgressList += new StreamingQueryProgressWrapper(info)
  }

  override def onQueryTerminated(event: StreamingQueryListener.QueryTerminatedEvent): Unit = {}
}
