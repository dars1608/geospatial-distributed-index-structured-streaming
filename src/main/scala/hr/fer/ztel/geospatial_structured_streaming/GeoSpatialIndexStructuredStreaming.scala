package hr.fer.ztel.geospatial_structured_streaming

import java.io.{File, FileInputStream, PrintWriter}
import java.text.SimpleDateFormat
import java.util.Date

import com.vividsolutions.jts.geom.Geometry
import hr.fer.ztel.geospatial_structured_streaming.config.EnvironmentWrapper
import hr.fer.ztel.geospatial_structured_streaming.config.IKeyWord._
import hr.fer.ztel.geospatial_structured_streaming.kafka.GeometryDeserializerWrapper
import hr.fer.ztel.geospatial_structured_streaming.stats.StreamingStatsListener
import org.apache.log4j.{Level, Logger}
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.functions.{struct, to_json}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.datasyslab.geospark.formatMapper.GeoJsonReader
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.{Adapter, GeoSparkSQLRegistrator}

object GeoSpatialIndexStructuredStreaming extends App {

  val env: EnvironmentWrapper = loadConfig(args)

  Logger.getLogger("org").setLevel(Level.toLevel(env.getProperty(LOGGING_LEVEL)))
  Logger.getLogger("akka").setLevel(Level.toLevel(env.getProperty(LOGGING_LEVEL)))
  Logger.getRootLogger.setLevel(Level.toLevel(env.getProperty(LOGGING_LEVEL)))

  val spark = createSparkSession(env)
  registerUdfs(spark)

  val statsListener = new StreamingStatsListener()
  spark.streams.addListener(statsListener)

  val subscriptionsDf = loadSubscriptions(spark, env)

  val kafkaIn = spark.readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", env.getProperty(STREAM_KAFKA_BROKERS_IN))
    .option("subscribe", env.getProperty(STREAM_KAFKA_TOPICS_IN))
    .option("startingOffsets", "latest")
    .load()


  kafkaIn
    .selectExpr("""deserialize_geometry("t", value) AS dataGeometry""", """deserialize_id("t", value) AS dataId""")
    .join(subscriptionsDf)
    .where("ST_Intersects(dataGeometry, subscriptionGeometry)")
    .select(to_json(struct("dataId", "subscriptionId")).alias("value"))
    .writeStream
    .format("kafka")
    .option("kafka.bootstrap.servers", env.getProperty(STREAM_KAFKA_BROKERS_OUT))
    .option("topic", env.getProperty(STREAM_KAFKA_TOPIC_OUT))
    .option("failOnDataLoss", value = false)
    .option("checkpointLocation", env.getProperty(CHECKPOINT_DIR))
    .start()
    .awaitTermination(1000L * env.getProperty(TIMEOUT).toLong)

  sys.addShutdownHook({
    statsListener.streamingProgressList.foreach(println)

    val fileName = String.format("%s_cache_%s_sp_part_%s_sp_idx_%s_num_part_%s.v2.tsv",
      new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date()),
      env.getProperty(SPARK_CACHE),
      env.getProperty(SPATIAL_PARTITIONING),
      env.getProperty(SPATIAL_INDEX),
      env.getProperty(NUM_OF_SPATIAL_PARTITIONS))


    val pw = new PrintWriter(new File(fileName))
    statsListener.streamingProgressList.foreach(pw.println)
    pw.flush()
    pw.close()
  })

  def loadConfig(args: Array[String]): EnvironmentWrapper = {
    val env = new EnvironmentWrapper()

    if (args != null && args.length == 1) {
      env.load(new FileInputStream(args(0)))
    } else {
      env.load(this.getClass.getResourceAsStream(CONFIG_FILE_DEFAULT_LOCATION))
    }
    env
  }

  def createSparkSession(env: EnvironmentWrapper): SparkSession = {
    val sparkSessionBuilder = SparkSession
      .builder()
      .appName(env.getProperty(SPARK_APP_NAME))
      .master(env.getProperty(SPARK_MASTER_URL))
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("geospark.join.gridtype", env.getProperty(SPATIAL_PARTITIONING).toLowerCase)
      .config("geospark.join.spatitionside", env.getProperty(SPATIAL_PARTITIONING_SIDE))

    val indexType = env.getProperty(SPATIAL_INDEX)
    val numOfPartitions = env.getProperty(NUM_OF_SPATIAL_PARTITIONS)
    if (indexType != null) {
      sparkSessionBuilder.config("geospark.global.index", value = true)
      sparkSessionBuilder.config("geospark.global.indextype", indexType)
      sparkSessionBuilder.config("geospark.join.indexbuildside", env.getProperty(INDEX_BUILD_SIDE))
    }

    if (numOfPartitions != null) {
      sparkSessionBuilder.config("geospark.join.numpartition", numOfPartitions.toInt)
    }

    sparkSessionBuilder.getOrCreate()
  }

  def registerUdfs(spark: SparkSession): Unit = {
    GeoSparkSQLRegistrator.registerAll(spark)
    spark.udf.register("deserialize_geometry", (topic: String, bytes: Array[Byte]) =>
      GeometryDeserializerWrapper.deserializer.deserialize(topic, bytes)
    )

    spark.udf.register("deserialize_id", (topic: String, bytes: Array[Byte]) =>
      GeometryDeserializerWrapper.deserializer.deserialize(topic, bytes).getUserData.asInstanceOf[String].toLong
    )
  }

  def loadSubscriptions(spark: SparkSession, env: EnvironmentWrapper): DataFrame = {
    val subscriptionsRDD = GeoJsonReader.readToGeometryRDD(
      spark.sparkContext, env.getProperty(SUBSCRIPTIONS_LOCATION),
      true,
      true
    )
    var subscriptionsDf = Adapter.toDf[Geometry](subscriptionsRDD, spark)
    subscriptionsDf = subscriptionsDf.selectExpr("ST_GeomFromWKT(geometry) AS subscriptionGeometry", "ID as subscriptionId")

    if (true.toString.equalsIgnoreCase(env.getProperty(SPARK_CACHE))) {
      subscriptionsDf.persist(StorageLevel.MEMORY_ONLY)
    }

    subscriptionsDf
  }
}
